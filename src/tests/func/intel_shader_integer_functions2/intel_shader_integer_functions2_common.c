// Copyright 2019 Intel Corporation
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice (including the next
// paragraph) shall be included in all copies or substantial portions of the
// Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#include "tapi/t.h"
#include "intel_shader_integer_functions2_common.h"

#include "src/tests/func/intel_shader_integer_functions2/intel_shader_integer_functions2_common-spirv.h"

/**
 * Generate results for an operator that is commutative.
 *
 * Commutative operators will only generate an upper-right triangular matrix
 * of results, and the diagonal will be missing.
 */
void
generate_results_commutative_no_diagonal(void *dest, const void *src_data,
                                         unsigned num_srcs, results_cb f)
{
    unsigned k = 0;

    for (unsigned i = 0; i < (num_srcs - 1); i++) {
        for (unsigned j = i + 1; j < num_srcs; j++) {
            f(dest, k, src_data, i, j);
            k++;
        }
    }
}

/**
 * Generate results for an operator that is commutative.
 *
 * Commutative operators will only generate an upper-right triangular matrix
 * of results, but the diagonal must also be explicitly stored.
 */
void
generate_results_commutative(void *dest, const void *src_data,
                             unsigned num_srcs, results_cb f)
{
    unsigned k = 0;

    for (unsigned i = 0; i < num_srcs; i++) {
        for (unsigned j = i; j < num_srcs; j++) {
            f(dest, k, src_data, i, j);
            k++;
        }
    }
}

/**
 * Generate full matrix of results without the diagonal.
 */
void
generate_results_no_diagonal(void *dest, const void *src_data,
                             unsigned num_srcs, results_cb f)
{
    unsigned k = 0;

    for (unsigned i = 0; i < num_srcs; i++) {
        for (unsigned j = 0; j < num_srcs; j++) {
            if (i != j) {
                f(dest, k, src_data, i, j);
                k++;
            }
        }
    }
}

void
run_integer_functions2_test(const QoShaderModuleCreateInfo *fs_info,
                            const void *src, unsigned src_size,
                            const void *expected, unsigned expected_size)
{
    t_require_ext("VK_INTEL_shader_integer_functions2");

    VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL int_func_features = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_FUNCTIONS_2_FEATURES_INTEL
    };

    VkPhysicalDeviceFeatures2 features = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
        .pNext = &int_func_features
    };

    vkGetPhysicalDeviceFeatures2(t_physical_dev, &features);

    if (!int_func_features.shaderIntegerFunctions2)
        t_skipf("shaderIntegerFunctions2 not supported");

    VkShaderModule vs = qoCreateShaderModuleGLSL(t_device, VERTEX,
        layout(location = 0) in vec4 a_position;
        void main()
        {
            gl_Position = a_position;
        }
    );

    VkShaderModule fs = __qoCreateShaderModule(t_device, fs_info);

    static const float vertices[8] = {
        -1.0, -1.0,
         1.0, -1.0,
        -1.0,  1.0,
         1.0,  1.0,
    };
    const unsigned vertices_offset = 0;

    const unsigned buffer_size = sizeof(vertices);

    VkBuffer buffer = qoCreateBuffer(t_device, .size = buffer_size,
        .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);

    VkDeviceMemory mem = qoAllocBufferMemory(t_device, buffer,
        .properties = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    qoBindBufferMemory(t_device, buffer, mem, 0);

    char *map = qoMapMemory(t_device, mem, 0, buffer_size, 0);
    memcpy(map + vertices_offset, vertices, sizeof(vertices));

    /* Setup the buffer that will hold the data for the fragment shader. */
    VkBuffer fs_buffer = qoCreateBuffer(t_device, .size = src_size + expected_size,
        .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT);

    VkDeviceMemory fs_mem = qoAllocBufferMemory(t_device, fs_buffer,
        .properties = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    qoBindBufferMemory(t_device, fs_buffer, fs_mem, 0);

    map = (char *) qoMapMemory(t_device, fs_mem, 0, src_size + expected_size, 0);
    memcpy(map, src, src_size);
    memcpy(map + src_size, expected, expected_size);

    VkDescriptorSetLayout set_layout = qoCreateDescriptorSetLayout(t_device,
        .bindingCount = 1,
        .pBindings = (VkDescriptorSetLayoutBinding[]) {
            {
                .binding = 0,
                .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .descriptorCount = 1,
                .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                .pImmutableSamplers = NULL,
            },
        });

    VkDescriptorSet set = qoAllocateDescriptorSet(t_device,
        .descriptorPool = t_descriptor_pool,
        .pSetLayouts = &set_layout);

    vkUpdateDescriptorSets(t_device,
        1, /* writeCount */
        &(VkWriteDescriptorSet) {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = set,
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .pBufferInfo = &(VkDescriptorBufferInfo) {
                .buffer = fs_buffer,
                .offset = 0,
                .range = src_size + expected_size,
            },
        }, 0, NULL);

    VkPipelineLayout pipeline_layout = qoCreatePipelineLayout(t_device,
        .setLayoutCount = 1,
        .pSetLayouts = &set_layout);

    VkPipelineVertexInputStateCreateInfo vi_create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &(VkVertexInputBindingDescription) {
            .binding = 0,
            .stride = 2 * sizeof(float),
            .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        },
        .vertexAttributeDescriptionCount = 1,
        .pVertexAttributeDescriptions = &(VkVertexInputAttributeDescription) {
            .location = 0,
            .binding = 0,
            .format = VK_FORMAT_R32G32_SFLOAT,
            .offset = vertices_offset,
        },
    };

    VkPipeline pipeline = qoCreateGraphicsPipeline(t_device, t_pipeline_cache,
        &(QoExtraGraphicsPipelineCreateInfo) {
            .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
            .vertexShader = vs,
            .fragmentShader = fs,
            .pNext =
        &(VkGraphicsPipelineCreateInfo) {
            .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            .pVertexInputState = &vi_create_info,
            .flags = 0,
            .layout = pipeline_layout,
            .renderPass = t_render_pass,
            .subpass = 0,
        }});

    vkCmdBeginRenderPass(t_cmd_buffer,
        &(VkRenderPassBeginInfo) {
            .renderPass = t_render_pass,
            .framebuffer = t_framebuffer,
            .renderArea = { { 0, 0 }, { t_width, t_height } },
            .clearValueCount = 1,
            .pClearValues = (VkClearValue[]) {
                { .color = { .float32 = { 1.0, 0.0, 0.0, 1.0 } } },
            }
        }, VK_SUBPASS_CONTENTS_INLINE);

    vkCmdBindVertexBuffers(t_cmd_buffer, 0, 1,
                           (VkBuffer[]) { buffer },
                           (VkDeviceSize[]) { vertices_offset });
    vkCmdBindPipeline(t_cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
    vkCmdBindDescriptorSets(t_cmd_buffer,
                            VK_PIPELINE_BIND_POINT_GRAPHICS,
                            pipeline_layout, 0, 1, &set, 0, NULL);
    vkCmdDraw(t_cmd_buffer, /*vertexCount*/ 4, /*instanceCount*/ 1,
              /*firstVertex*/ 0, /*firstInstance*/ 0);
    vkCmdEndRenderPass(t_cmd_buffer);
    qoEndCommandBuffer(t_cmd_buffer);
    qoQueueSubmit(t_queue, 1, &t_cmd_buffer, VK_NULL_HANDLE);
}
